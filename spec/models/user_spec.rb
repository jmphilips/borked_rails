require 'rails_helper'

RSpec.describe User, type: :model do
  it 'is invalid without an email address' do
    expect(User.new).to_not be_valid
  end

  it 'is valid with an email address' do
    expect(User.new({ email: 'test@test.com' })).to be_valid
  end
end

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)


## Creates Generic User and an Account to go along with it.
user1 = User.create({ email: 'useremail@email.com' })
Account.create({ user: user1, institution: 'Ledger' })

## Creates me and an Account for me :)
user2 = User.create({ email: 'jmichaelphilips@gmail.com' })
Account.create({ user: user2, institution: 'Ledger'})

# Creates asset for bitcoin
Asset.create({ symbol: 'BTC', name: 'bitcoin' })

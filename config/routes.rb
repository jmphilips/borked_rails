Rails.application.routes.draw do
  root 'users#new'
  get 'home/index'
  get 'signup', to: 'users#new'
  get 'accounts/index', to: 'accounts#index'
  resources :accounts, only: %i[new create]
  resources :users, only: [:create]
end

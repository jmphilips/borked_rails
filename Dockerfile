# Use the official Ruby 3.2.2 image as the base image
FROM ruby:3.2.2

# Set the working directory inside the container
WORKDIR /borked_rails

# Install the required dependencies
RUN apt-get update && apt-get install -y \
  build-essential \
  libpq-dev

# Copy the Gemfile and Gemfile.lock files to the container
COPY Gemfile Gemfile.lock ./

# Install the project dependencies
RUN gem install bundler:2.2.27
RUN bundle install --jobs 4 --retry 3

# Copy the entire project directory to the container
COPY . .

# Expose the port on which the Rails app will run (default is 3000)
EXPOSE 3000

# Start the Rails server
CMD ["bundle", "exec", "rails", "s", "-b", "0.0.0.0"]

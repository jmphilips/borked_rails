# frozen_string_literal: true

class AccountsController < ApplicationController
  def index
    @accounts = Account.where({ user_id: session[:user_id] })
  end

  def show
    @account = Account.find(params[:id])
  end

  def new
    @account = Account.new({ user_id: session[:user_id] })
  end

  def create
    @account = Account.new(account_params)
    @account.user_id = session[:user_id]
    @account.save
    redirect_to '/accounts/index'
  end

  private

  def account_params
    params.require(:account).permit(:institution)
  end
end
